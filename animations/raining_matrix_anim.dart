
import 'package:SpiDev/animations/models/matrix_list.dart';
import 'package:flutter/material.dart';
import 'dart:math';

import 'package:random_string/random_string.dart';

class RainingMatrixAnim extends StatefulWidget {
  RainingMatrixAnim(this.context);
  final BuildContext context;
  @override
  State<StatefulWidget> createState() => _RainingMatrixAnim();
}

class _RainingMatrixAnim extends State<RainingMatrixAnim>
    with TickerProviderStateMixin {
  AnimationController _controller;
  AnimationController _controller2;
  AnimationController _controller3;
  AnimationController _controller4;
  AnimationController _controller5;

  Animation<double> _animation;
  Animation<double> _animation2;
  Animation<double> _animation3;
  Animation<double> _animation4;
  Animation<double> _animation5;

  List<Matrix> list1 = new List<Matrix>();
  List<Matrix> list2 = new List<Matrix>();
  List<Matrix> list3 = new List<Matrix>();
  List<Matrix> list4 = new List<Matrix>();
  List<Matrix> list5 = new List<Matrix>();

  double height;

  @override
  void initState() {
    // _initApp();
    super.initState();

    height = MediaQuery.of(widget?.context).size.height;
    _initApp();
  }

  _initApp() {
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(
        milliseconds: 3000,
      ),
    );

    _controller2 = AnimationController(
      vsync: this,
      duration: const Duration(
        milliseconds: 3000,
      ),
    );

    _controller3 = AnimationController(
      vsync: this,
      duration: const Duration(
        milliseconds: 3000,
      ),
    );

    _controller4 = AnimationController(
      vsync: this,
      duration: const Duration(
        milliseconds: 4000,
      ),
    );

    _controller5 = AnimationController(
      vsync: this,
      duration: const Duration(
        milliseconds: 3000,
      ),
    );

    _initAnim();

    _animation.addListener(() {
      setSymbol();
      setState(() {});
    });

    _animation2.addListener(() {
      setSymbol2();
      setState(() {});
    });

    _animation3.addListener(() {
      setSymbol3();
      setState(() {});
    });

    _animation4.addListener(() {
      setSymbol4();
      setState(() {});
    });

    _animation5.addListener(() {
      setSymbol5();
      setState(() {});
    });

    _controller.forward();
    _controller2.forward();
    _controller3.forward();
    _controller4.forward();
    _controller5.forward();
  }

  void _initAnim() {
    final r1 = Random().nextDouble() * height;
    final r2 = Random().nextDouble() * height;
    final r3 = Random().nextDouble() * height;
    final r4 = Random().nextDouble() * height;
    final r5 = Random().nextDouble() * height;

    _animation = Tween<double>(
      begin: -r1,
      end: height + r1,
    ).animate(_controller);

    _animation2 = Tween<double>(
      begin: -r2,
      end: height + r2,
    ).animate(_controller2);

    _animation3 = Tween<double>(
      begin: -r3,
      end: height + r3,
    ).animate(_controller3);

    _animation4 = Tween<double>(
      begin: -r4,
      end: height + r4,
    ).animate(_controller4);

    _animation5 = Tween<double>(
      begin: -r5,
      end: height + r5,
    ).animate(_controller5);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: <Widget>[
          list1.isNotEmpty
              ? Transform.translate(
                  offset: Offset(list1[0].x, _animation?.value),
                  child: ListView.builder(
                    itemCount: list1.length,
                    scrollDirection: Axis.vertical,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      return Opacity(
                        opacity: index / 2 * 0.1,
                        child: Text(list1[index].symbol,
                            style: TextStyle(
                                fontSize: 20, color: Colors.green.shade600)),
                      );
                    },
                  ),
                )
              : Container(),
          list2.isNotEmpty
              ? Transform.translate(
                  offset: Offset(list2[0].x, _animation2?.value),
                  child: ListView.builder(
                    itemCount: list2.length,
                    scrollDirection: Axis.vertical,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      return Text(list2[index].symbol,
                          style: TextStyle(
                              fontSize: 20, color: Colors.green.shade600));
                    },
                  ),
                )
              : Container(),
          list3.isNotEmpty
              ? Transform.translate(
                  offset: Offset(list3[0].x, _animation3?.value),
                  child: ListView.builder(
                    itemCount: list3.length,
                    scrollDirection: Axis.vertical,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      return Text(list3[index].symbol,
                          style: TextStyle(
                              fontSize: 20, color: Colors.green.shade600));
                    },
                  ),
                )
              : Container(),
          list4.isNotEmpty
              ? Transform.translate(
                  offset: Offset(list4[0].x, _animation4?.value),
                  child: ListView.builder(
                    itemCount: list4.length,
                    scrollDirection: Axis.vertical,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      return Text(list4[index].symbol,
                          style: TextStyle(fontSize: 20, color: Colors.green.shade600));
                    },
                  ),
                )
              : Container(),
          list5.isNotEmpty
              ? Transform.translate(
                  offset: Offset(list5[0].x, _animation5?.value),
                  child: ListView.builder(
                    itemCount: list5.length,
                    scrollDirection: Axis.vertical,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      return Text(list5[index].symbol,
                          style: TextStyle(fontSize: 20, color: Colors.green.shade600));
                    },
                  ),
                )
              : Container(),
        ],
      ),
    );
  }

  @override
  dispose() {
    super.dispose();
    _controller.removeStatusListener((status) {});
    _controller.dispose();
    _controller = null;
  }

  String getSymbol() {
    return randomString(1);
  }

  double getRandom() {
    return Random().nextDouble() * MediaQuery.of(context).size.width;
  }

  void setSymbol() {
    if (list1.length < 20) {
      list1.add(Matrix(getSymbol(), getRandom(), _animation?.value));
    }

    if (_animation.isCompleted) {
      list1.clear();
      _controller.reset();
      _controller.forward();
    }
  }

  void setSymbol2() {
    if (list2.length < 20) {
      list2.add(Matrix(getSymbol(), getRandom(), _animation2?.value));
    }

    if (_animation2.isCompleted) {
      list2.clear();
      _controller2.reset();
      _controller2.forward();
    }
  }

  void setSymbol3() {
    if (list3.length < 20) {
      list3.add(Matrix(getSymbol(), getRandom(), _animation3?.value));
    } else {
      list3[list3.length - 1] =
          Matrix(getSymbol(), getRandom(), _animation3?.value);
    }

    if (_animation3.isCompleted) {
      list3.clear();
      _controller3.reset();
      _controller3.forward();
    }
  }

  void setSymbol4() {
    if (list4.length < 20) {
      list4.add(Matrix(getSymbol(), getRandom(), _animation4?.value));
    } else {
      list4[list4.length - 1] =
          Matrix(getSymbol(), getRandom(), _animation4?.value);
    }

    if (_animation4.isCompleted) {
      list4.clear();
      _controller4.reset();
      _controller4.forward();
    }
  }

  void setSymbol5() {
    if (list5.length < 20) {
      list5.add(Matrix(getSymbol(), getRandom(), _animation5?.value));
    } else {
      list5[list5.length - 1] =
          Matrix(getSymbol(), getRandom(), _animation5?.value);
    }

    if (_animation5.isCompleted) {
      list5.clear();
      _controller5.reset();
      _controller5.forward();
    }
  }
}
